---
title: Workspace
image: '/images/workspace-desk.jpg'
---

Interesting how many gears I have gathered over time, mostly in the past 3 years. Most of them are paid in part or full by my employers. Some are backups, especially in a presentation where things can fail without warning.


**Desk**

- [Ikea SKARSTA desk](https://www.ikea.com/nl/en/p/skarsta-desk-sit-stand-white-s89324812/)
- [Herman Miller Aeron remastered office chair](https://store.hermanmiller.com/office/office-chairs/aeron-chair/2195348.html?lang=en_US#lang=en_US&start=1)

**Laptop**

- MacBook Pro (16-inch, 2019) (Main)
- [Google Pixelbook Go](https://www.amazon.com/Google-Pixelbook-Chromebook-128GB-Black/dp/B07YMM4YC1) (For Outdoors)

**Keyboard**

- [Keychron K2 v2](https://www.keychron.com/products/keychron-k2-wireless-mechanical-keyboard) (Main, Mechanical)
- [Logitech MX Keys](https://www.logitech.com/nl-nl/products/keyboards/mx-keys-wireless-keyboard.html?crid=27)
- [Logitech K780](https://www.logitech.com/nl-nl/products/keyboards/k780-multi-device-wireless-keyboard.html?crid=27)
- [Logitech K380](https://www.logitech.com/nl-nl/products/keyboards/k380-multi-device.html?crid=27) (Always in my bag, for outdoors)

**Mouse**

- [Apple Magic Trackpad](https://www.apple.com/shop/product/MJ2R2LL/A/magic-trackpad-2-silver) (Previously main use but was moved to my left for those moments when I need to person Mac-specific gestures)
- [Logitech MX Vertical](https://www.logitech.com/nl-nl/products/mice/mx-vertical-ergonomic-mouse.910-005448.html?crid=7) (Alternative, fixed my wrist pain)
- [Logitech MX Master 3](https://www.logitech.com/nl-nl/products/mice/mx-master-3.910-005694.html?crid=7) (Main)
- [Logitech Pebble M350](https://www.logitech.com/nl-nl/products/mice/m350-pebble-wireless-mouse.910-005718.html?crid=7) (Outdoor)

**Audio**

- [Elgato Wave 3](https://www.elgato.com/en/wave-3) (Main Mic)
- [Behringer C3 Condenser microphone](https://www.behringer.com/product.html?modelCode=P0262)
- [Behringer UMC202HD Audio interface](https://www.behringer.com/product.html?modelCode=P0BJZ)
- [Logitech Z533 Speaker system with subwoofer](https://www.logitech.com/nl-nl/product/multimedia-speakers-z533?crid=47) (Main Speaker)
- [Bose QC 35 II](https://www.bose.com/en_us/products/headphones/over_ear_headphones/quietcomfort-35-wireless-ii.html#v=qc35_ii_silver)

**Video**

- Sony a6000 Mirrorless camera
- [Sigma 16mm f1.4 lens](https://www.sigma-global.com/en/lenses/c017_16_14/)
- [Feelworld T7 Field Monitor 7inch](http://www.feelworld.cn/ShowInfo.aspx?id=530&py=FEELWOR)
- [TeleprompterPAD iLight Pro 10 inch](https://www.teleprompterpad.com/en/producto/pack-teleprompter-pad-ilight-pro-10-black-ipad-android-bluetooth-remote-controller/)
- [Logitech c920 HD](https://www.logitech.com/nl-nl/products/webcams/c920-pro-hd-webcam.960-001055.html?crid=34) (Backup webcam)
- [Elgato Camlink](https://www.elgato.com/en/cam-link-4k)
- [ATEM Mini](https://www.blackmagicdesign.com/nl/products/atemmini)

**Lighting**

- [Elgato Key Light Air](https://www.elgato.com/en/key-light-air) (Key light)
- [Viltrox L132T](https://www.amazon.nl/VILTROX-professionele-ultradunne-LED-videolamp-fotografie/dp/B01LVWEKW8/ref=asc_df_B01LVWEKW8/?tag=nlshogostdde-21&linkCode=df0&hvadid=430558678075&hvpos=&hvnetw=g&hvrand=15336707170135076782&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9064840&hvtargid=pla-385947285775&psc=1)
- [Wix Smart light](https://www.wizconnected.com/)

**Other Accessories**

- [Caldigit TS3 Plus Dock](https://www.caldigit.com/ts3-plus/)
- [Logitech Spotlight Wireless Presentation Remote](https://www.logitech.com/nl-nl/product/spotlight-presentation-remote)
- [Google Nest mini](https://store.google.com/product/google_nest_mini)