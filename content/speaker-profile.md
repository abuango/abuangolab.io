---
title: Speaker Profile
image: '/images/speaker.jpg'
---

Abubakar Siddiq Ango is a Developer Evangelism Program Manager at GitLab, where he speaks, writes and shares about GitLab & Cloud Native technologies. He is a Certifies Kubernetes Administrator, Certified Kubernetes Security Specialist and [AltSchool](https://www.altschoolafrica.com/) Content Partner/Instructor for Cloud Programme.  When not working, he engages with the community through the CNCF, Google Developers and other developer communities. He is also a Google Developer Expert (Cloud) and a CNCF Ambassador.

**Twitter:** [https://www.twitter.com/sarki247](https://www.twitter.com/sarki247) 

**LinkedIn:** [https://www.linkedin.com/in/abubakarango/](https://www.linkedin.com/in/abubakarango/)

**Email:** abubakar@gitlab.com / hello@abuango.me

**GitLab Profile**: [https://about.gitlab.com/company/team/#abuango](https://about.gitlab.com/company/team/#abuango)

**Personal Website:** [https://abuango.me](https://abuango.me)


### Profile Photos

You can select a photo that meets your requirement from this [shared Google drive](https://drive.google.com/drive/folders/1DJ4ORp1H8x4_96-uzgd7x3z7AhzVmjZU?usp=sharing).

### Previous Talks

TBD
