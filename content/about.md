---
title: About
image: '/images/about-lagos.jpg'
---

Abubakar Siddiq Ango is a Developer Evangelism Program Manager at GitLab, where he finds better ways for the Developer Evangelism team to engage with the community and measure its impact. He is a Certified Kubernetes Security Specialist and when not working, he engages with the community through the CNCF, Google Developer & Hashicorp user groups and other developer communities. He is also the Lead Organizer of the Kubernetes Community Days Africa (KCD Africa) and a Google Developer Expert (Cloud).

### Recognitions

<img style="margin-bottom:40px; width:250px;" src="/images/experts-digital-badge-logos-2023_cloud.png" />


### Credly Recognitions

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="1a1c62fa-ceb6-4772-a7e5-e4c0bc4af73c" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="bd6bc419-a384-4ab7-b080-6cb0ea3864e2" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="79bdfc60-dcf4-4035-938f-7e258d52d4a3" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="8dfa4473-0da4-4f65-8dc6-344fc9fd893e" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="66e8c8c0-3236-4bbe-99ef-ebcbd45efaf5" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="2c11ca57-8308-4bf8-a752-331656f995a8" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>


  