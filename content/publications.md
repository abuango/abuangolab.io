---
title: Publications
image: '/images/publications.jpg'
---

- [How to use GitLab with OCI ARM-based compute instances](https://about.gitlab.com/blog/2021/05/25/gitlab-oracle-cloud-arm-based/) - *[GitLab Blog](https://about.gitlab.com/blog/)*
- [How to collect form data from a JAMStack website to Google Sheets using Google Cloud Functions](https://medium.com/@abuango/how-to-collect-form-data-from-a-jamstack-website-to-google-sheets-using-google-cloud-functions-a59546c803a5) - *[Medium](https://medium.com/@abuango)*
- [How to create a Kubernetes cluster on Amazon EKS in GitLab](https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/) - *[GitLab Blog](https://about.gitlab.com/blog/)*
- [How to deploy your application to a GitLab-managed Amazon EKS cluster with Auto DevOps](https://about.gitlab.com/blog/2020/05/05/deploying-application-eks/) - *[GitLab Blog](https://about.gitlab.com/blog/)*
- [How to use GitLab with GKE Autopilot](https://about.gitlab.com/blog/2021/02/24/gitlab-gke-autopilot/) - *[GitLab Blog](https://about.gitlab.com/blog/)*
- [Webinar] [Using DevOps to navigate the implications of COVID-19](https://www.bigmarker.com/closerstill-media/Using-DevOps-to-navigate-the-implications-of-COVID-19?utm_bmcr_source=Abubakar)