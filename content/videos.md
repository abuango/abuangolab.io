---
title: Videos
image: '/images/videos.jpg'
---

- [CNCF Webinar: Securing your continuous everything strategy](https://www.youtube.com/watch?v=NhTp7_6xBwk)
- [CNCF Webinar: Understanding GitOps Use-cases](https://www.youtube.com/watch?v=VspKKg0_s-Q)
- [HashiCorp EU 2021: Orchestration to Delivery: Integrating GitLab with HashiCorp Terraform, Packer, Vault, and Waypoint.](https://www.youtube.com/watch?v=cMqmvaByxNI)
- [KubeCon EU 2021: Improving the Impact of Diversity Initiatives in Africa](https://www.youtube.com/watch?v=UY5h0GAp7Ho)
- [How to use GitLab with OCI ARM-based compute instances](https://www.youtube.com/watch?v=Q2o0JYdQAWE)
- [How to install GitLab on GKE Autopilot (Part 1)](https://www.youtube.com/watch?v=cNffh-qyXhQ&list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ&index=4)
- [How to Integrate a GKE Autopilot Cluster with GitLab](https://www.youtube.com/watch?v=rCwHL3hQEWU&list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ&index=3)
- [Dutch GitLab Meetup: From Idea to Production: Using GitLab for your whole development lifecycle](https://www.youtube.com/watch?v=SSGfCU5FaF4&t=84s)
- [Community Day: Continuous Integration - Session 1 with Abubakar Siddiq Ango](https://www.youtube.com/watch?v=avPF9Ds6eCI)
- [RailsGirls Kumasi - Intro to Git & GitLab](https://www.youtube.com/watch?v=E5pe2jrOZpc&t=5s)
- [KubeCon EU 2020: Why We Are Choosing Cloud Native Buildpacks at GitLab - Abubakar Siddiq, GitLab](https://www.youtube.com/watch?v=oTC-itx6ubE&t=73s)
- [Tanzu Tuesdays - Building security into development pipelines with GitLab with Abubakar Siddiq Ango](https://www.youtube.com/watch?v=zCvj8rSvWPA)
- [HashiTalks Africa 2020: Can Your CI/CD Pipeline Keep a Secret?](https://www.youtube.com/watch?v=Z3ds_XwWGos)
- [Cloud Native Summit Online](https://www.youtube.com/watch?v=jv69-lSc1aA&list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ&index=31)