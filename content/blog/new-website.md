---
title:  My new website
date:   2023-01-29 15:55:35 +0100
image:  '/images/writing-pen.jpg'
tags:   [personal]
---
I have been through several iterations of my personal website over the past 15 years. Started with a WordPress blog, which changed a number of times, then moved to [Medium](https://medium.com/@abuango). But Medium just didn't click for me and the search went on for a home on the web for me. Now, I am here with [Hugo](https://gohugo.io/).

While Hugo isn't my first static site generator (SSG), I like its simplicity. I have worked with it on projects for communities I worked with like the [Inclusive Naming Initiative](https://inclusivenaming.org/). Other SSGs & platforms I have tried are [Notion](https://www.notion.so/),  [Nanoc](https://nanoc.app/about/) and [Middleman](https://middlemanapp.com/).

The goal of this website is to have a destination online where I document my learnings, journeys and lessons learnt.