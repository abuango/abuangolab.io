---
title: Homelab
image: '/images/homelab-servers.jpg'
---

In my spare time, I experiment with virtualization using VMWare ESXi and Proxmox VE, on a HP DL360 G9 and Dell Precision 5820 respectively.