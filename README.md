# Joseph – minimal content focus theme for Hugo

Joseph is a super minimal content focus theme for Hugo. This theme was created with a focus on typography and a reading experience and has a light and dark mode for easy reading. This theme is perfect for the personal blog of a programmer, designer, blogger, or any creative person.

* * *

### Demo

Check the theme in action [Live Demo](https://joseph-hugo.netlify.app/) |
[Anvod Studio](https://anvodstudio.com)

* * *

### Theme features

- Dark and light mode user can select themself
- 100% responsive and clean theme
- Optimized for mobile devices
- Super fast performance ⚡⚡⚡
- No jQuery, only vanilla JS
- Social sharing buttons
- Scroll to top button
- Syntax highlighting
- Instant Search
- Compatible with modern browsers
- Medium style image zoom
- Tags Support
- Custom logo support
- Supports contact form (Formspree)
- Supports MailChimp newsletter
- Supports Disqus comments
- Supports Google Analytics
- Ionicons icons
- Free Google Fonts
- Free Updates

* * *

### Deployment

To run the theme locally, navigate to the theme directory and run `hugo` to start building websites, then run `hugo server` or `hugo server --disableFastRender` to start the HUGO server.

I would recommend checking the [Deployment Methods](https://gohugo.io/hosting-and-deployment/) page on HUGO website.


* * *

### Documentation

Before using the Joseph theme, please read the attached documentation.